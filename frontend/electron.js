const path = require('path');
const url = require('url');
const { app, BrowserWindow, Menu, shell } = require('electron');
const isDev = require('electron-is-dev');

let win;

const createWindow = () => {
  win = new BrowserWindow({
    width: 1100,
    height: 800,
    title: 'Notes',
    webPreferences: {
      // not the most secure
      nodeIntegration: true,
    },
  });

  if (isDev) {
    const location = 'http://localhost:3000';
    win.loadURL(location);
  } else {
    const location = url.format({
      pathname: path.join(__dirname, 'build', 'index.html'),
      protocol: 'file:',
      slashes: true,
    });
    win.loadURL(location);
  }

  if (isDev) {
    // win.webContents.openDevTools();
  }

  win.on('closed', () => {
    win = null;
  });
};

const generateMenu = () => {
  const template = [
    {
      label: 'File',
      submenu: [
        {
          label: 'About Notes',
          selector: 'orderFrontStandardAboutPanel:',
        },
        { type: 'separator' },
        {
          label: 'Hide Notes',
          accelerator: 'Command+H',
          selector: 'hide:',
        },
        {
          label: 'Hide Others',
          accelerator: 'Command+Shift+H',
          selector: 'hideOtherApplications:',
        },
        { label: 'Show All', selector: 'unhideAllApplications:' },
        { type: 'separator' },
        {
          label: 'Quit',
          accelerator: 'Command+Q',
          click: () => {
            app.quit();
          },
        },
      ],
    },
    {
      role: 'Window',
      submenu: [{ role: 'minimize' }, { role: 'close' }],
    },
    {
      role: 'Help',
      submenu: [
        {
          click() {
            shell.openExternal('https://www.mctighecw.net');
          },
          label: 'Learn More',
        },
      ],
    },
  ];

  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
};

app.on('ready', () => {
  createWindow();
  generateMenu();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});
