import moment from 'moment';
import sanitizeHtml from 'sanitize-html';
const KEY_HASH = process.env.KEY_HASH;

const getOffset = (datestring) => {
  const offset = moment().utcOffset();
  const currentDateTime = moment
    .utc(datestring)
    .utcOffset(offset)
    .format();
  return currentDateTime;
};

export const formatDateTime = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime).format('DD MMM YYYY, hh:mm A');
};

export const getFilename = (item) => {
  return item.slice(0, item.length - 4);
};

export const getExtension = (item) => {
  return item.substr(item.length - 3);
};

export const sanitizeText = (input) => {
  const cleanHtml = sanitizeHtml(input, {
    allowedTags: ['b', 'i', 'u', 'br'],
    allowedAttributes: {
      a: [],
      img: [],
    },
    selfClosing: ['br'],
    allowedSchemes: [],
    allowedSchemesByTag: {},
    allowedSchemesAppliedToAttributes: [],
    allowProtocolRelative: false,
  });

  return cleanHtml;
};

export const setUser = (user) => {
  localStorage.setItem(KEY_HASH, user);
};

export const getUser = () => {
  const user = localStorage.getItem(KEY_HASH);
  return user;
};

export const clearUser = () => {
  localStorage.removeItem(KEY_HASH);
};
