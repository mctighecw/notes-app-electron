const devUrl = 'http://localhost:9100';
const local = '';
const remote = 'https://notes-app.mctighecw.site';
const prodUrl = process.env.ELECTRON === 'yes' ? remote : local;

export const getPrefix = () => {
  const prefix = process.env.NODE_ENV === 'development' ? devUrl : prodUrl;
  return prefix;
};

export const getAuthUrl = (type) => {
  const prefix = getPrefix();
  const apiUrl = 'api/auth';
  return `${prefix}/${apiUrl}/${type}`;
};

export const getAttachmentUrl = (type) => {
  const prefix = getPrefix();
  const apiUrl = 'api/attachment';
  return `${prefix}/${apiUrl}/${type}`;
};
