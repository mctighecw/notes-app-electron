import React, { useRef, useState, useEffect } from 'react';
import TextEditor from 'Components/TextEditor/TextEditor';
import TextBox from 'Components/TextBox/TextBox';
import './styles.scss';

const NewNote = () => {
  const textAreaRef = useRef();
  const [category, setCategory] = useState('');
  const [userInput, setUserInput] = useState('');

  const handleSetCategory = (item) => {
    let value = item;
    if (category === item) value = '';
    setCategory(value);
  };

  const handleAdd = (item, type) => {
    const textArea = textAreaRef.current;
    const { selectionStart, selectionEnd, value } = textArea;
    const before = value.substring(0, selectionStart);
    const middle =
      type === 'tag' ? `<${item}>${value.substring(selectionStart, selectionEnd)}</${item}>` : item;
    const after = value.substring(selectionEnd, value.length);
    const newInput = before + middle + after;

    setUserInput(newInput);
    if (type === 'symbol') setCategory('');
  };

  return (
    <div styleName="container">
      <div styleName="heading">New Note</div>

      <div styleName="content">
        <TextEditor category={category} handleAdd={handleAdd} handleSetCategory={handleSetCategory} />

        <TextBox ref={textAreaRef} userInput={userInput} setUserInput={setUserInput} />
      </div>
    </div>
  );
};

export default NewNote;
