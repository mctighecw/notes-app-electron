import React, { useState } from 'react';
import { Image, CloudinaryContext } from 'cloudinary-react';
import { formatDate, formatDateTime, getFilename, getExtension } from 'Utils/functions';

import { useMutation } from '@apollo/react-hooks';
import { GET_NOTES } from 'Graphql/queries';
import { UPDATE_NOTE, DELETE_NOTE } from 'Graphql/mutations';

import ArchiveIcon from 'Assets/archive-black.svg';
import TrashIcon from 'Assets/trash-black.svg';
import MoveUpIcon from 'Assets/move-up.svg';
import PDFIcon from 'Assets/pdf.svg';
import ImageModal from 'Components/ImageModal/ImageModal';
import './styles.scss';

const cloudName = process.env.CLOUD_NAME;

const Note = (props) => {
  const { item, type } = props;
  const { id, content, attachments, createdAt } = item;

  const [showModal, setShowModal] = useState({ show: false, item: '' });

  const displayRichText = () => {
    return { __html: content };
  };

  const [updateNote] = useMutation(UPDATE_NOTE, {
    refetchQueries: [
      {
        query: GET_NOTES,
      },
    ],
  });

  const [deleteNote] = useMutation(DELETE_NOTE, {
    refetchQueries: [
      {
        query: GET_NOTES,
      },
    ],
  });

  return (
    <div styleName="container">
      {type === 'notes' ? (
        <div styleName="actions">
          <img
            src={ArchiveIcon}
            alt="Archive"
            onClick={(event) =>
              updateNote({
                variables: {
                  id,
                  status: 'archive',
                },
              })
            }
          />
          <img
            src={TrashIcon}
            alt="Delete"
            onClick={(event) =>
              updateNote({
                variables: {
                  id,
                  status: 'trash',
                },
              })
            }
          />
        </div>
      ) : (
        <div styleName="actions">
          <img
            src={MoveUpIcon}
            alt="Active"
            onClick={(event) =>
              updateNote({
                variables: {
                  id,
                  status: 'active',
                },
              })
            }
          />
          {type === 'archive' ? (
            <img
              src={TrashIcon}
              alt="Delete"
              onClick={(event) =>
                updateNote({
                  variables: {
                    id,
                    status: 'trash',
                  },
                })
              }
            />
          ) : (
            <img
              src={TrashIcon}
              alt="Delete"
              onClick={(event) =>
                deleteNote({
                  variables: {
                    id,
                  },
                })
              }
            />
          )}
        </div>
      )}
      <div styleName="date">{formatDateTime(createdAt)}</div>
      <div styleName="content">
        <div dangerouslySetInnerHTML={displayRichText()} />
      </div>
      <div styleName="attachments">
        <CloudinaryContext cloudName={cloudName} secure={true}>
          {attachments.length > 0 &&
            attachments.map((item, index) => {
              const filename = getFilename(item);
              const extension = getExtension(item);
              const path = `notes/attachments/${filename}`;

              if (extension === 'pdf') {
                return (
                  <img
                    key={index}
                    src={PDFIcon}
                    alt="PDF"
                    styleName="item pdf"
                    onClick={() => setShowModal({ show: true, item })}
                  />
                );
              } else {
                return (
                  <Image
                    key={index}
                    publicId={path}
                    crop="scale"
                    styleName="item image"
                    onClick={() => setShowModal({ show: true, item })}
                  />
                );
              }
            })}
        </CloudinaryContext>
      </div>

      {showModal.show && (
        <ImageModal item={showModal.item} onClickClose={() => setShowModal({ show: false, item: '' })} />
      )}
    </div>
  );
};

export default Note;
