import React, { useState } from 'react';
import './styles.scss';

const Pagination = ({ totalPages, currentPage, onClickMethod }) => {
  const renderPagination = () => {
    const items = ['<', ...Array(totalPages).keys(), '>'];
    const divs = [];

    items.map((item, index) => {
      const page = typeof item === 'number' ? item + 1 : item;
      const disabledItem =
        (item === '<' && currentPage === 1) || (item === '>' && currentPage === totalPages);
      const disabled = disabledItem ? 'disabled' : '';
      const style = page === currentPage ? 'page active' : `page ${disabled}`;

      divs.push(
        <div key={page} styleName={style} onClick={() => onClickMethod(page)}>
          {page}
        </div>
      );
    });
    return divs;
  };

  return <div styleName="container">{renderPagination()}</div>;
};

export default Pagination;
