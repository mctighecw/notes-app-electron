import React from 'react';
import axios from 'axios';
import { Link, withRouter } from 'react-router-dom';
import client from 'Graphql/client';
import { getAuthUrl } from 'Utils/network';
import { getUser, clearUser } from 'Utils/functions';
import { useQuery } from '@apollo/react-hooks';
import menuItems from './menuItems';

import ProfileIcon from 'Assets/profile.svg';
import LogoutIcon from 'Assets/logout.svg';
import InfoWhite from 'Assets/info.svg';
import InfoGold from 'Assets/info-gold.svg';
import './styles.scss';

const SideMenu = ({ location, history }) => {
  const user = getUser();

  const renderMenu = () => {
    const divs = menuItems.map((item, index) => (
      <Link to={item.path} key={index} styleName="link">
        <img src={item.path === location.pathname ? item.active : item.icon} alt={item.label} />
        <div styleName={item.path === location.pathname ? 'text active' : 'text'}>{item.label}</div>
      </Link>
    ));
    return divs;
  };

  const handleLogout = () => {
    const url = getAuthUrl('logout');

    axios({
      method: 'GET',
      headers: { 'Content-type': 'application/x-www-form-urlencoded' },
      withCredentials: true,
      url,
    })
      .then((res) => {
        clearUser();
        client.resetStore();
        history.push('/login');
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div styleName="container">
      <div styleName="content">
        <div styleName="user">
          <img src={ProfileIcon} alt={user} styleName="user-icon" />
          <div styleName="username">{user}</div>
          <img src={LogoutIcon} alt="Logout" styleName="logout-icon" onClick={handleLogout} />
        </div>

        <div styleName="menu-items">{renderMenu()}</div>

        <div styleName="info">
          <Link to="/info" styleName="link">
            <img src={'/info' === location.pathname ? InfoGold : InfoWhite} alt="Info" />
            <div styleName={'/info' === location.pathname ? 'text active' : 'text'}>Info</div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default withRouter(SideMenu);
