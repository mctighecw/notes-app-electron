import NewWhite from 'Assets/new.svg';
import NotesWhite from 'Assets/notes.svg';
import ArchiveWhite from 'Assets/archive.svg';
import TrashWhite from 'Assets/trash.svg';

import NewGold from 'Assets/new-gold.svg';
import NotesGold from 'Assets/notes-gold.svg';
import ArchiveGold from 'Assets/archive-gold.svg';
import TrashGold from 'Assets/trash-gold.svg';

const menuItems = [
  {
    label: 'New',
    icon: NewWhite,
    active: NewGold,
    path: '/new',
  },
  {
    label: 'Notes',
    icon: NotesWhite,
    active: NotesGold,
    path: '/notes',
  },
  {
    label: 'Archive',
    icon: ArchiveWhite,
    active: ArchiveGold,
    path: '/archive',
  },
  {
    label: 'Trash',
    icon: TrashWhite,
    active: TrashGold,
    path: '/trash',
  },
];

export default menuItems;
