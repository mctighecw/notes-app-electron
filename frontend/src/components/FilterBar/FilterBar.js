import React, { useState } from 'react';
import SearchField from 'Components/shared/inputs/SearchField/SearchField';
import DropDown from 'Components/shared/inputs/DropDown/DropDown';
import SmallDropDown from 'Components/shared/inputs/SmallDropDown/SmallDropDown';
import './styles.scss';

const FilterBar = (props) => {
  const { searchValue, sortValue, maxValue, setSearchValue, setSortValue, setMaxValue } = props;
  const sortOptions = ['newest', 'oldest'];
  const maxOptions = ['2', '4', '6', '8', '10'];

  return (
    <div styleName="container">
      <div styleName="search">
        <div styleName="label">Search</div>
        <SearchField
          value={searchValue}
          maxLength={20}
          onChangeMethod={(event) => setSearchValue(event.target.value)}
          onClickClear={(event) => setSearchValue('')}
        />
      </div>

      <div styleName="order-by">
        <div styleName="label">Order by</div>
        <DropDown
          value={sortValue}
          options={sortOptions}
          onChangeMethod={(event) => setSortValue(event.target.value)}
        />
      </div>

      <div styleName="show-max">
        <div styleName="label">Show max</div>
        <SmallDropDown
          value={maxValue}
          options={maxOptions}
          onChangeMethod={(event) => setMaxValue(event.target.value)}
        />
      </div>
    </div>
  );
};

export default FilterBar;
