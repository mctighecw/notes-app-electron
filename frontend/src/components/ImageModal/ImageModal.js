import React from 'react';
import { Image, CloudinaryContext } from 'cloudinary-react';
import { getFilename, getExtension } from 'Utils/functions';

import GoldButton from 'Components/shared/buttons/Gold/Gold';
import CloseIcon from 'Assets/close.svg';
import styles from './styles.scss';

const cloudName = process.env.CLOUD_NAME;

const ImageModal = ({ item, onClickClose }) => {
  const filename = getFilename(item);
  const extension = getExtension(item);

  const path = `notes/attachments/${filename}`;
  const style = extension === 'pdf' ? 'pdf' : 'image';

  return (
    <div styleName="modal-background">
      <div styleName="box">
        <img src={CloseIcon} alt="" styleName="close" onClick={onClickClose} />

        <div styleName="content">
          <CloudinaryContext cloudName={cloudName} secure={true}>
            <Image publicId={path} crop="scale" styleName={style} />
          </CloudinaryContext>

          <div styleName="button">
            <GoldButton label="Close" type="full" onClickMethod={onClickClose} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ImageModal;
