import React from 'react';
import GoldButton from 'Components/shared/buttons/Gold/Gold';
import CloseIcon from 'Assets/close.svg';
import styles from './styles.scss';

const ConfirmModal = ({ text, cancelLabel, confirmLabel, onClickCancel, onClickConfirm }) => (
  <div styleName="modal-background">
    <div styleName="box">
      <img src={CloseIcon} alt="" styleName="close" onClick={onClickCancel} />

      <div styleName="content">
        <div styleName="text">{text}</div>

        <div styleName="buttons">
          <GoldButton label={cancelLabel} type="outline" onClickMethod={onClickCancel} />
          <GoldButton label={confirmLabel} type="full" onClickMethod={onClickConfirm} />
        </div>
      </div>
    </div>
  </div>
);

export default ConfirmModal;
