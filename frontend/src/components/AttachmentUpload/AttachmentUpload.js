import React, { useState } from 'react';
import axios from 'axios';
import { useDropzone } from 'react-dropzone';
import { Image, CloudinaryContext } from 'cloudinary-react';
import { getFilename, getExtension } from 'Utils/functions';
import { getAttachmentUrl } from 'Utils/network';

import AddFileIcon from 'Assets/add-file.svg';
import PDFIcon from 'Assets/pdf.svg';
import LoadingSpinner from 'Assets/loading-spinner.svg';
import './styles.scss';

const cloudName = process.env.CLOUD_NAME;

const AttachmentUpload = ({ attachments, setAttachments }) => {
  const [uploading, setUploading] = useState(false);

  const uploadAttachment = (formData) => {
    const url = getAttachmentUrl('upload');

    setUploading(true);

    axios({
      method: 'POST',
      headers: { 'Content-Type': 'multipart/form-data' },
      withCredentials: true,
      url,
      data: formData,
    })
      .then((res) => {
        const { filename } = JSON.parse(res.request.response);
        let currentAttachments = [...attachments];
        currentAttachments.push(filename);
        setAttachments(currentAttachments);
        setUploading(false);
      })
      .catch((err) => {
        console.log(err);
        setUploading(false);
      });
  };

  const deleteAttachment = (filename, index) => {
    const url = getAttachmentUrl('delete');
    const data = JSON.stringify({ filename });

    axios({
      method: 'POST',
      headers: { 'Content-type': 'application/x-www-form-urlencoded' },
      withCredentials: true,
      url,
      data,
    })
      .then((res) => {
        console.log(res);
        let currentAttachments = [...attachments];
        currentAttachments.splice(index, 1);
        setAttachments(currentAttachments);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onDrop = (files) => {
    const formData = new FormData();
    formData.append('file', files[0]);
    uploadAttachment(formData);
  };

  const handleDelete = (index) => {
    const currentAttachments = [...attachments];
    const filename = getFilename(currentAttachments[index]);
    deleteAttachment(filename, index);
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: 'application/pdf, image/png, image/gif, image/jpeg',
    multiple: true,
    onDrop,
  });

  return (
    <div styleName="container">
      <div styleName="drop-zone">
        <div {...getRootProps()}>
          <input {...getInputProps()} />
          <img src={AddFileIcon} alt="Add" />
          {isDragActive ? <p>Drop here</p> : <p>PDF - PNG - GIF - JPG</p>}
        </div>
      </div>
      <div styleName="attachments">
        <div styleName="box">
          <CloudinaryContext cloudName={cloudName} secure={true}>
            {attachments.length > 0 &&
              attachments.map((item, index) => {
                const filename = getFilename(item);
                const extension = getExtension(item);
                const path = `notes/attachments/${filename}`;

                return (
                  <div key={index} styleName="item">
                    <div
                      styleName={extension === 'pdf' ? 'delete indent' : 'delete left'}
                      onClick={() => handleDelete(index)}
                    >
                      x
                    </div>
                    {extension === 'pdf' ? (
                      <img key={index} src={PDFIcon} alt="PDF" styleName="pdf" />
                    ) : (
                      <Image publicId={path} crop="scale" styleName="image" />
                    )}
                  </div>
                );
              })}
            {attachments.length === 0 && !uploading && <div styleName="no-attachments">NO ATTACHMENTS</div>}
            {uploading && (
              <div styleName="uploading">
                <img src={LoadingSpinner} alt="Uploading" />
              </div>
            )}
          </CloudinaryContext>
        </div>
      </div>
    </div>
  );
};

export default AttachmentUpload;
