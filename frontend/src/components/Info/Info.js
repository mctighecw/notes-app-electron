import React from 'react';
import TechIcons from './TechIcons/TechIcons';
import Footer from './Footer/Footer';
import { links } from './constants';
import './styles.scss';

const Info = () => {
  const renderLink = (name) => {
    const link = links[name];

    return (
      <a href={link.url} target="_blank">
        {link.label}
      </a>
    );
  };

  return (
    <div styleName="container">
      <div styleName="heading">Info</div>

      <div styleName="box">
        <div styleName="sub-heading">Overview</div>
        <div styleName="paragraph">
          With this app, users can create notes, search/ filter them, move them to the archive, or delete
          them.
        </div>

        <div styleName="paragraph">
          A note can have bold, italic, or underlined text. In addition, various special symbols and emojis
          can be added.
        </div>

        <div styleName="paragraph">
          A picture or PDF can also be attached to the note. A preview of each attachment is displayed on the
          bottom; clicking on it will open a modal that has a larger view.
        </div>

        <div styleName="sub-heading extra-margin-top">Tech Stack</div>
        <div styleName="paragraph">
          There are two versions available: one that is entirely web-based and another that is a desktop
          app, using {renderLink('electron')}.
        </div>

        <div styleName="paragraph">
          This app uses a modern tech stack: {renderLink('react')}, {renderLink('flask')}, {renderLink('gql')}
          , and {renderLink('mongodb')}.
        </div>

        <TechIcons />
        <Footer />
      </div>
    </div>
  );
};

export default Info;
