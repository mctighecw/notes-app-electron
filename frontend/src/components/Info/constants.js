import ElectronLogo from 'Assets/tech/electron.svg';
import ReactLogo from 'Assets/tech/react.svg';
import PythonLogo from 'Assets/tech/python.svg';
import GraphQLLogo from 'Assets/tech/graphql.svg';
import MongoDBLogo from 'Assets/tech/mongodb.svg';

export const links = {
  electron: { label: 'Electron', url: 'https://electronjs.org' },
  react: { label: 'React', url: 'https://reactjs.org' },
  flask: { label: 'Python Flask', url: 'https://palletsprojects.com/p/flask' },
  gql: { label: 'GraphQL', url: 'https://graphql.org' },
  mongodb: { label: 'MongoDB', url: 'https://www.mongodb.com' },
};

export const icons = [
  { label: 'Electron', icon: ElectronLogo },
  { label: 'React', icon: ReactLogo },
  { label: 'Python Flask', icon: PythonLogo },
  { label: 'GraphQL', icon: GraphQLLogo },
  { label: 'MongoDB', icon: MongoDBLogo },
];
