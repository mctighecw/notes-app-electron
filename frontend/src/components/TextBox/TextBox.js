import React, { forwardRef, useState } from 'react';
import { sanitizeText } from 'Utils/functions';

import { useMutation } from '@apollo/react-hooks';
import { GET_NOTES } from 'Graphql/queries';
import { ADD_NOTE } from 'Graphql/mutations';

import TextArea from 'Components/shared/inputs/TextArea/TextArea';
import AttachmentUpload from 'Components/AttachmentUpload/AttachmentUpload';
import GoldButton from 'Components/shared/buttons/Gold/Gold';
import ConfirmModal from 'Components/ConfirmModal/ConfirmModal';

import './styles.scss';

const TextBox = forwardRef((props, ref) => {
  const { userInput, setUserInput } = props;

  const [textBox, setTextBox] = useState('edit');
  const [rows, setRows] = useState(4);
  const [attachments, setAttachments] = useState([]);
  const [showModal, setShowModal] = useState(false);

  const handleOnKeyPress = (event) => {
    if (event.which == 13 || event.keyCode == 13) {
      const newTag = '<br />';
      const newInput = userInput + newTag;
      setUserInput(newInput);
      setRows(rows + 1);
    }
  };

  const displayRichText = () => {
    const cleanHtml = sanitizeText(userInput);
    return { __html: cleanHtml };
  };

  const toggleTextBox = () => {
    const newValue = textBox === 'preview' ? 'edit' : 'preview';
    setTextBox(newValue);
  };

  const handleConfirmClear = () => {
    if (userInput !== '') setShowModal(true);
  };

  const handleClearInput = () => {
    setShowModal(false);
    setUserInput('');
    setRows(4);
  };

  const handleSave = () => {
    addNote();
    setUserInput('');
    setTextBox('edit');
    setAttachments([]);
  };

  const [addNote] = useMutation(ADD_NOTE, {
    variables: {
      content: sanitizeText(userInput),
      attachments: attachments.length > 0 ? attachments : [],
    },
    refetchQueries: [
      {
        query: GET_NOTES,
      },
    ],
  });

  return (
    <div styleName="container">
      {textBox === 'edit' ? (
        <TextArea
          ref={ref}
          value={userInput}
          rows={rows}
          autoFocus={true}
          onFocusMethod={(event) =>
            event.target.setSelectionRange(event.target.value.length, event.target.value.length)
          }
          onChangeMethod={(event) => setUserInput(event.target.value)}
          onKeyPressMethod={handleOnKeyPress}
        />
      ) : (
        <div styleName="preview-box">
          <div dangerouslySetInnerHTML={displayRichText()} />
        </div>
      )}

      <AttachmentUpload attachments={attachments} setAttachments={setAttachments} />

      <div styleName={`buttons ${textBox}`}>
        {textBox === 'preview' && <GoldButton type="full" label="Save note" onClickMethod={handleSave} />}

        <GoldButton
          type={textBox === 'edit' ? 'full' : 'outline'}
          label={textBox === 'edit' ? 'Go to preview' : 'Back to editing'}
          disabled={userInput === ''}
          onClickMethod={toggleTextBox}
        />

        {textBox === 'edit' && (
          <GoldButton
            type="outline"
            label="Clear"
            disabled={userInput === ''}
            onClickMethod={handleConfirmClear}
          />
        )}
      </div>

      {showModal && (
        <ConfirmModal
          text="Are you sure that you want to delete all of the text?"
          cancelLabel="Cancel"
          confirmLabel="Delete text"
          onClickCancel={() => setShowModal(false)}
          onClickConfirm={handleClearInput}
        />
      )}
    </div>
  );
});

export default TextBox;
