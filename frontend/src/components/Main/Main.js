import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import SideMenu from 'Components/SideMenu/SideMenu';
import NewNote from 'Components/NewNote/NewNote';
import NotesBox from 'Components/NotesBox/NotesBox';
import Info from 'Components/Info/Info';

const Main = () => (
  <div>
    <SideMenu />
    <Switch>
      <Route path="/new" component={NewNote} />
      <Route path="/notes" component={NotesBox} />
      <Route path="/archive" component={NotesBox} />
      <Route path="/trash" component={NotesBox} />
      <Route path="/info" component={Info} />
      <Route render={() => <Redirect to="/notes" />} />
    </Switch>
  </div>
);

export default Main;
