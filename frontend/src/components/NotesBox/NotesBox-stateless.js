import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';

import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_NOTES } from 'Graphql/queries';
import { DELETE_NOTES } from 'Graphql/mutations';

import FilterBar from 'Components/FilterBar/FilterBar';
import Pagination from 'Components/Pagination/Pagination';
import RedButton from 'Components/shared/buttons/Red/Red';
import Note from 'Components/Note/Note';
import LoadingMessage from 'Components/LoadingMessage/LoadingMessage';
import ErrorMessage from 'Components/ErrorMessage/ErrorMessage';
import ConfirmModal from 'Components/ConfirmModal/ConfirmModal';

import './styles.scss';

/*
  React stateless functional component version of NotesBox.
  The large number of useState hooks make it a bit messy and
  confusing.
*/

const NotesBox = (props) => {
  const { loading, error, data } = useQuery(GET_NOTES);
  const [searchValue, setSearchValue] = useState('');
  const [sortValue, setSortValue] = useState('newest');
  const [maxValue, setMaxValue] = useState(4);
  const [currentPage, setCurrentPage] = useState(1);
  const [showModal, setShowModal] = useState(false);
  const { pathname } = props.location;
  const type = pathname.slice(1);

  const renderNotes = (type, totalPages, data) => {
    const start = (currentPage - 1) * maxValue;
    const stop = currentPage * maxValue;

    if (type !== 'notes') {
      data = data.filter((item) => item.status === type);
    } else {
      data = data.filter((item) => item.status === 'active');
    }

    if (searchValue.length > 2) {
      data = data.filter((item) => item.content.toLowerCase().includes(searchValue.toLowerCase()));
    }

    if (sortValue === 'newest') {
      data.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
    } else {
      data.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
    }

    data = data.slice(start, stop);

    if (data.length === 0) {
      return <div styleName="no-notes">No notes</div>;
    } else {
      return data.map((item, index) => <Note key={index} item={item} type={type} />);
    }
  };

  const handleSetCurrentPage = (value, totalPages) => {
    let page = null;

    if (value === '<' && currentPage !== 1) {
      page = currentPage - 1;
    } else if (value === '>' && currentPage !== totalPages) {
      page = currentPage + 1;
    } else if (typeof value === 'number') {
      page = value;
    }
    if (page !== null) setCurrentPage(page);
  };

  const handleChangeMaxValue = (value) => {
    const maxValue = parseInt(value, 10);

    setCurrentPage(1);
    setMaxValue(maxValue);
  };

  const handleDeleteAll = () => {
    setShowModal(false);
    deleteNotes();
  };

  const [deleteNotes] = useMutation(DELETE_NOTES, {
    refetchQueries: [
      {
        query: GET_NOTES,
      },
    ],
  });

  useEffect(() => {
    setCurrentPage(1);
  }, [pathname]);

  const trash =
    data && data.allNotes && data.allNotes.length > 0
      ? data.allNotes.filter((item) => item.status === 'trash')
      : [];
  const trashNotEmpty = trash.length > 0;

  const notesFilter = type === 'notes' ? 'active' : type;

  const totalPages =
    data && data.allNotes
      ? Math.ceil(data.allNotes.filter((item) => item.status === notesFilter).length / maxValue)
      : 1;

  return (
    <div styleName="container">
      <div styleName="heading">{type}</div>

      <FilterBar
        searchValue={searchValue}
        sortValue={sortValue}
        maxValue={maxValue}
        setSearchValue={setSearchValue}
        setSortValue={setSortValue}
        setMaxValue={handleChangeMaxValue}
      />

      {totalPages > 1 && (
        <Pagination
          totalPages={totalPages}
          currentPage={currentPage}
          onClickMethod={(value) => handleSetCurrentPage(value, totalPages)}
        />
      )}

      {type === 'trash' && trashNotEmpty && (
        <div styleName="delete-button">
          <RedButton type="full" label="Delete all" onClickMethod={() => setShowModal(true)} />
        </div>
      )}

      {loading && <LoadingMessage />}
      {error && <ErrorMessage />}
      {data && data.allNotes && <div styleName="notes">{renderNotes(type, totalPages, data.allNotes)}</div>}

      {showModal && (
        <ConfirmModal
          text="Are you sure that you want to delete all notes in the trash?"
          cancelLabel="Cancel"
          confirmLabel="Delete all"
          onClickCancel={() => setShowModal(false)}
          onClickConfirm={handleDeleteAll}
        />
      )}
    </div>
  );
};

export default withRouter(NotesBox);
