import React from 'react';
import { withRouter } from 'react-router-dom';

import { Query, Mutation } from 'react-apollo';
import { GET_NOTES } from 'Graphql/queries';
import { DELETE_NOTES } from 'Graphql/mutations';

import FilterBar from 'Components/FilterBar/FilterBar';
import Pagination from 'Components/Pagination/Pagination';
import RedButton from 'Components/shared/buttons/Red/Red';
import Note from 'Components/Note/Note';
import LoadingMessage from 'Components/LoadingMessage/LoadingMessage';
import ErrorMessage from 'Components/ErrorMessage/ErrorMessage';
import ConfirmModal from 'Components/ConfirmModal/ConfirmModal';

import './styles.scss';

/*
  React stateful component version of NotesBox.
  This seems better organized and easier to understand than the
  stateless functional version.
*/

class NotesBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: '',
      sortValue: 'newest',
      maxValue: 4,
      currentPage: 1,
      showModal: false,
    };
  }

  renderNotes = (type, totalPages, data) => {
    const { searchValue, sortValue, maxValue, currentPage } = this.state;
    const start = (currentPage - 1) * maxValue;
    const stop = currentPage * maxValue;

    if (type !== 'notes') {
      data = data.filter((item) => item.status === type);
    } else {
      data = data.filter((item) => item.status === 'active');
    }

    if (searchValue.length > 2) {
      data = data.filter((item) => item.content.toLowerCase().includes(searchValue.toLowerCase()));
    }

    if (sortValue === 'newest') {
      data.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
    } else {
      data.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
    }

    data = data.slice(start, stop);

    if (data.length === 0) {
      return <div styleName="no-notes">No notes</div>;
    } else {
      return data.map((item, index) => <Note key={index} item={item} type={type} />);
    }
  };

  handleSetCurrentPage = (value, totalPages) => {
    const { currentPage } = this.state;
    let page = null;

    if (value === '<' && currentPage !== 1) {
      page = currentPage - 1;
    } else if (value === '>' && currentPage !== totalPages) {
      page = currentPage + 1;
    } else if (typeof value === 'number') {
      page = value;
    }
    if (page !== null) this.setState({ currentPage: page });
  };

  handleDeleteAll = (deleteNotes) => {
    this.setState({ showModal: false });
    deleteNotes();
  };

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.setState({ currentPage: 1 });
    }
  }

  render() {
    const { searchValue, sortValue, maxValue, currentPage, showModal } = this.state;
    const type = this.props.location.pathname.slice(1);

    return (
      <Query query={GET_NOTES} onCompleted={(data) => console.log(`${data.allNotes.length} notes received`)}>
        {({ loading, error, data }) => {
          if (loading) return <LoadingMessage />;
          if (error) return <ErrorMessage />;

          const trash =
            data.allNotes.length > 0 ? data.allNotes.filter((item) => item.status === 'trash') : [];
          const trashNotEmpty = trash.length > 0;

          const notesFilter = type === 'notes' ? 'active' : type;

          const totalPages = Math.ceil(
            data.allNotes.filter((item) => item.status === notesFilter).length / maxValue
          );

          return (
            <div styleName="container">
              <div styleName="heading">{type}</div>

              <FilterBar
                searchValue={searchValue}
                sortValue={sortValue}
                maxValue={maxValue}
                setSearchValue={(value) => this.setState({ searchValue: value })}
                setSortValue={(value) => this.setState({ sortValue: value })}
                setMaxValue={(value) => {
                  const maxValue = parseInt(value, 10);
                  this.setState({ maxValue, currentPage: 1 });
                }}
              />

              {totalPages > 1 && (
                <Pagination
                  totalPages={totalPages}
                  currentPage={currentPage}
                  onClickMethod={(value) => this.handleSetCurrentPage(value, totalPages)}
                />
              )}

              {type === 'trash' && trashNotEmpty && (
                <div styleName="delete-button">
                  <RedButton
                    type="full"
                    label="Delete all"
                    onClickMethod={() => this.setState({ showModal: true })}
                  />
                </div>
              )}

              {data.allNotes && (
                <div styleName="notes">{this.renderNotes(type, totalPages, data.allNotes)}</div>
              )}

              {showModal && (
                <Mutation
                  mutation={DELETE_NOTES}
                  onCompleted={(data) => console.log(`Notes deleted: ${data.deleteNotes.count}`)}
                  refetchQueries={() => {
                    return [
                      {
                        query: GET_NOTES,
                      },
                    ];
                  }}
                >
                  {(deleteNotes) => (
                    <ConfirmModal
                      text="Are you sure that you want to delete all notes in the trash?"
                      cancelLabel="Cancel"
                      confirmLabel="Delete all"
                      onClickCancel={() => this.setState({ showModal: false })}
                      onClickConfirm={() => this.handleDeleteAll(deleteNotes)}
                    />
                  )}
                </Mutation>
              )}
            </div>
          );
        }}
      </Query>
    );
  }
}

export default withRouter(NotesBox);
