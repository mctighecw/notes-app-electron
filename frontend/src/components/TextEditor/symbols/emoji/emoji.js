import faces from './faces';
import foods from './foods';
import flags from './flags';

const emoji = {
  '😊': faces,
  '🍔': foods,
  '🇸🇯': flags,
};

export default emoji;
