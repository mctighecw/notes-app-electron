import international from './international';
const { greek, european } = international;

const characters = {
  'Σ': greek,
  'Æ': european,
};

export default characters;
