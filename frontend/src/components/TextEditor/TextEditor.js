import React, { useState } from 'react';
import characters from './symbols/characters/characters';
import emoji from './symbols/emoji/emoji';
import './styles.scss';

const TextEditor = ({ category, handleSetCategory, handleAdd }) => {
  const renderCategories = () => {
    const symbols = ['b', 'i', 'u'];
    const divs = [];

    symbols.map((item, index) => {
      divs.push(
        <div key={index} styleName={`tag ${item}`} onClick={() => handleAdd(item, 'tag')}>
          {item}
        </div>
      );
    });
    return divs;
  };

  const renderSymbols = () => {
    if (category !== '') {
      const cKeys = Object.keys(characters);
      const source = cKeys.includes(category) ? characters : emoji;
      const style = cKeys.includes(category) ? 'character' : 'emoji';

      return source[category].map((item, index) => (
        <div key={index} styleName={style} onClick={() => handleAdd(item, 'symbol')}>
          {item}
        </div>
      ));
    }
  };

  return (
    <div styleName="container">
      <div styleName="box">
        <div styleName="row">
          {renderCategories()}

          <div styleName="heading">Symbols:</div>
          {Object.keys(characters).map((item, index) => (
            <div
              key={index}
              styleName={category === item ? 'category active' : 'category'}
              onClick={() => handleSetCategory(item)}
            >
              {item}
            </div>
          ))}

          <div styleName="heading">Emoji:</div>
          {Object.keys(emoji).map((item, index) => (
            <div
              key={index}
              styleName={category === item ? 'category active' : 'category'}
              onClick={() => handleSetCategory(item)}
            >
              {item}
            </div>
          ))}
        </div>

        <div styleName="symbols-box">
          <div styleName="content">{renderSymbols()}</div>
        </div>
      </div>
    </div>
  );
};

export default TextEditor;
