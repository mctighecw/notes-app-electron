import React from 'react';
import CloseCircleIcon from 'Assets/close-circle.svg';
import './styles.scss';

const SearchField = ({ value, maxLength, disabled, onKeyPress, onChangeMethod, onClickClear }) => (
  <div styleName="container">
    <input
      type="text"
      value={value}
      maxLength={maxLength}
      disabled={disabled || false}
      onKeyPress={onKeyPress}
      onChange={onChangeMethod}
    />
    <div styleName="clear">{value !== '' && <img src={CloseCircleIcon} onClick={onClickClear} />}</div>
  </div>
);

export default SearchField;
