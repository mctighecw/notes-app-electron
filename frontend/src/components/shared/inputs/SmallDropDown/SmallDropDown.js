import React from 'react';
import './styles.scss';

const SmallDropDown = ({ value, options, onChangeMethod }) => (
  <div styleName="small-dropdown">
    <select value={value} onChange={onChangeMethod}>
      {options.length > 0 &&
        options.map((item, index) => (
          <option value={item} key={index}>
            {item}
          </option>
        ))}
    </select>
  </div>
);

export default SmallDropDown;
