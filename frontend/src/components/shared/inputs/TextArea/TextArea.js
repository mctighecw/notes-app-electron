import React, { forwardRef } from 'react';
import './styles.scss';

const TextArea = forwardRef((props, ref) => {
  const { value, rows, autoFocus, onFocusMethod, onChangeMethod, onKeyPressMethod } = props;

  return (
    <textarea
      ref={ref}
      value={value}
      rows={rows}
      autoFocus={autoFocus}
      styleName="text-area"
      onFocus={onFocusMethod}
      onChange={onChangeMethod}
      onKeyPress={onKeyPressMethod}
    />
  );
});

export default TextArea;
