import React from 'react';
import './styles.scss';

const Red = ({ label, type, disabled, onClickMethod }) => {
  const style = `button ${type}`;

  return (
    <div styleName="container">
      <button styleName={style} disabled={disabled} onClick={onClickMethod}>
        {label}
      </button>
    </div>
  );
};

export default Red;
