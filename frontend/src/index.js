import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter, HashRouter } from 'react-router-dom';
import client from './graphql/client';
import Root from './Root';

// Electron works better with the HashRouter
const Router = process.env.ELECTRON === 'yes' ? HashRouter : BrowserRouter;

ReactDOM.render(
  <ApolloProvider client={client}>
    <Router>
      <Root />
    </Router>
  </ApolloProvider>,
  document.getElementById('root')
);
