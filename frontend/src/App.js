import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { getUser } from 'Utils/functions';
import './styles/global.css';

import SignUp from 'Components/Auth/SignUp/SignUp';
import Login from 'Components/Auth/Login/Login';
import Main from 'Components/Main/Main';

class App extends React.Component {
  handleCheckRouting = () => {
    const { history, location } = this.props;
    const user = getUser();

    if (!user && location.pathname !== '/login' && location.pathname !== '/sign-up') {
      history.replace({
        pathname: '/login',
        state: { from: location },
      });
    }

    if (user && (location.pathname === '/login' || location.pathname === '/sign-up')) {
      history.replace({
        pathname: '/notes',
        state: { from: location },
      });
    }
  };

  componentDidMount() {
    this.handleCheckRouting();
  }

  render() {
    return (
      <Switch>
        <Route path="/sign-up" component={SignUp} />
        <Route path="/login" component={Login} />
        <Route component={Main} />
      </Switch>
    );
  }
}

export default withRouter(App);
