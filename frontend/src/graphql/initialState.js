const initialState = {
  users: {
    __typename: 'UserType',
  },
  allNotes: {
    __typename: 'NoteType',
  },
};

export default initialState;
