import gql from 'graphql-tag';

const typeDefs = `
  type UserType {
    id: String!
    username: String!
    email: String!
  }
  type NoteType {
    id: String!
    content: String!
    status: String!
    user: String!
    attachments: List
    createdAt: String!
    deletedAt: String
  }
  type Query {
    allNotes: [NoteType]
  }
  type Mutation {
    addNote: [NoteType]
    updateNote: [NoteType]
    deleteNote: [NoteType]
    deleteNotes: [NoteType]
  }
`;

export default typeDefs;
