import gql from 'graphql-tag';

export const ADD_NOTE = gql`
  mutation addNote($content: String!, $attachments: [String]) {
    addNote(content: $content, attachments: $attachments) {
      note {
        id
      }
    }
  }
`;

export const UPDATE_NOTE = gql`
  mutation updateNote($id: String!, $status: String!) {
    updateNote(id: $id, status: $status) {
      note {
        id
      }
    }
  }
`;

export const DELETE_NOTE = gql`
  mutation deleteNote($id: String!) {
    deleteNote(id: $id) {
      note {
        id
      }
    }
  }
`;

export const DELETE_NOTES = gql`
  mutation deleteNotes {
    deleteNotes {
      count
    }
  }
`;
