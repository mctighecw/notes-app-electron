import gql from 'graphql-tag';

export const GET_NOTES = gql`
  query {
    allNotes {
      id
      content
      status
      attachments
      createdAt
    }
  }
`;
