import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { getPrefix } from 'Utils/network';

import typeDefs from './typeDefs';
import initialState from './initialState';

const prefix = getPrefix();
const uri = `${prefix}/api/graphql`;
const link = createHttpLink({ uri, credentials: 'include' });
const cache = new InMemoryCache();

cache.writeData({ data: initialState });

const client = new ApolloClient({
  link,
  cache,
  typeDefs,
  resolvers: {},
});

export default client;
