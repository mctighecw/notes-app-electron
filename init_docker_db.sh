#!/bin/sh

echo Initializing database in container...

docker exec -it notes-app-electron_backend_1 python ./src/db/init_db.py
