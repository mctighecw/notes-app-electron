import graphene
from graphene_mongo import MongoengineObjectType
from datetime import datetime, timezone
from flask_jwt_extended import get_jwt_identity

from src.models import (User as UserModel,
                        Note as NoteModel)


# types
class UserType(MongoengineObjectType):
    class Meta:
        model = UserModel

class NoteType(MongoengineObjectType):
    class Meta:
        model = NoteModel


# queries
class Query(graphene.ObjectType):
    """
    All GraphQL queries.
    """
    users = graphene.List(UserType)
    all_notes = graphene.List(NoteType)

    def resolve_users(self, info):
        query = UserModel.objects()
        return query

    def resolve_all_notes(self, info):
        user = get_jwt_identity()
        query = NoteModel.objects.filter(user=user, status__ne='deleted').all()
        return query


# mutations
class AddNote(graphene.Mutation):
    """
    GraphQL mutation to add a new note.
    """
    class Arguments:
        content = graphene.String()
        attachments = graphene.List(graphene.String)

    note = graphene.Field(lambda: NoteType)

    def mutate(self, info, content, attachments):
        user = get_jwt_identity()

        new_note = NoteModel(
            content=content,
            user=user,
            attachments=attachments,
            created_at=datetime.now(timezone.utc).astimezone()
        )
        new_note.save()

        return AddNote(note=new_note)


class UpdateNote(graphene.Mutation):
    """
    GraphQL mutation to update note status (archive or trash).
    """
    class Arguments:
        id = graphene.String()
        status = graphene.String()

    note = graphene.Field(lambda: NoteType)

    def mutate(self, info, id, status):
        query = NoteModel.objects.get(pk=id)
        query.status = status
        query.save()

        return UpdateNote(note=query)


class DeleteNote(graphene.Mutation):
    """
    GraphQL mutation to delete a single note in the trash.
    """
    class Arguments:
        id = graphene.String()

    note = graphene.Field(lambda: NoteType)

    def mutate(self, info, id):
        query = NoteModel.objects.get(pk=id)
        query.deleted_at = datetime.now(timezone.utc).astimezone()
        query.status = 'deleted'
        query.save()

        return DeleteNote(note=query)


class DeleteNotes(graphene.Mutation):
    """
    GraphQL mutation to delete all notes in the trash.
    """
    count = graphene.Int()

    def mutate(self, info):
        user = get_jwt_identity()
        count = NoteModel.objects(user=user, status='trash').count()
        NoteModel.objects(user=user, status='trash').update(
            set__status = 'deleted',
            set__deleted_at = datetime.now(timezone.utc).astimezone()
        )

        return DeleteNotes(count=count)


class Mutation(graphene.ObjectType):
    """
    All GraphQL mutations.
    """
    addNote = AddNote.Field()
    updateNote = UpdateNote.Field()
    deleteNote = DeleteNote.Field()
    deleteNotes = DeleteNotes.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
