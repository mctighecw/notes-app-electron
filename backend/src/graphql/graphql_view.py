import os

from flask_jwt_extended import jwt_required
from flask_graphql import GraphQLView
from src.graphql.schema import schema

FLASK_ENV = os.getenv('FLASK_ENV')

if FLASK_ENV == 'development':
    allow_graphiql = True
else:
    allow_graphiql = False

def graphql_view():
    view = GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=allow_graphiql
    )

    return jwt_required(view)
