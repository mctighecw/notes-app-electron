import os
import sys

root_dir = os.path.abspath(os.curdir)
sys.path.append(root_dir)

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_mongoengine import MongoEngine

from src import flask_config
from seed_db import seed_db

def initialize_db():
    """
    Minimal Flask app to connect to MongoDB and initialize database.
    """
    app = Flask(__name__)
    app.config.from_object(flask_config)
    db = MongoEngine(app)
    seed_db()

initialize_db()
