users_data = [{
    'username': 'jsmith',
    'email': 'john@smith.com',
    'password': 'password'
},
{
    'username': 'swilliams',
    'email': 'susan@williams.com',
    'password': 'password'
}]

notes_data = [{
    'content': 'Great vacation in 🇸🇯<br />😁',
    'status': 'active',
    'user': 'jsmith'
  },
  {
    'content': 'Recommend <b>Burger Shop</b> to friends 🍔🍺',
    'status': 'active',
    'user': 'jsmith'
  },
  {
    'content': 'I have to sort out my <i>emails</i> 😐',
    'status': 'active',
    'user': 'swilliams'
  },
  {
    'content': 'Call and make appointment for dentist 😒',
    'status': 'active',
    'user': 'swilliams'
  },
  {
    'content': 'Plan next vacation to 🇯🇵',
    'status': 'active',
    'user': 'swilliams'
  },
  {
    'content': 'Type up meeting notes',
    'status': 'archive',
    'user': 'jsmith'
  },
  {
    'content': 'Schedule doctor appointment',
    'status': 'archive',
    'user': 'jsmith'
  },
  {
    'content': 'Clean out closet',
    'status': 'archive',
    'user': 'swilliams'
  },
  {
    'content': 'Organize boxes in basement',
    'status': 'archive',
    'user': 'swilliams'
  },
  {
    'content': 'Wash car',
    'status': 'trash',
    'user': 'jsmith'
  },
  {
    'content': 'Repair back door',
    'status': 'trash',
    'user': 'jsmith'
  },
  {
    'content': 'Get washing machine checked',
    'status': 'trash',
    'user': 'jsmith'
  },
  {
    'content': 'Polish bookshelf',
    'status': 'trash',
    'user': 'swilliams'
  },
  {
    'content': 'Have TV fixed',
    'status': 'trash',
    'user': 'swilliams'
  },
  {
    'content': 'Do some laundry',
    'status': 'trash',
    'user': 'swilliams'
 }]
