import os
import sys

root_dir = os.path.abspath(os.curdir)
sys.path.append(root_dir)

from flask_bcrypt import Bcrypt
from src.models import User, Note
from seeds import users_data, notes_data

bcrypt = Bcrypt()

def seed_db():
    """
    Seeds database with initial data
    """
    for item in users_data:
        print(item['username'])
        password_hash = bcrypt.generate_password_hash(item['password']).decode('utf-8')
        new_user = User(
            username=item['username'],
            email=item['email'],
            password=password_hash
        )
        new_user.save()

    for item in notes_data:
        print(item['content'])

        new_note = Note(
            content=item['content'],
            status=item['status'],
            user=item['user']
        )
        new_note.save()
