import os
import pymongo

client = pymongo.MongoClient(
    'localhost',
    port=27017,
    username='admin',
    password='mypassword',
    authSource='admin')

print('Dropping notes database collections...')

db = client.get_database('notes')
users = db.users
data = db.data

users.drop()
data.drop()
