import os, json, datetime, random, string

import cloudinary
import cloudinary.uploader
from src.env_config import FLASK_ENV, CLOUD_NAME, CLOUDINARY_KEY, CLOUDINARY_SECRET

from flask import request, Blueprint, Response
from flask_cors import CORS
from flask_jwt_extended import jwt_required
from werkzeug.utils import secure_filename

from src.helpers import get_file_extension, allowed_file_upload

attachment = Blueprint("attachment", __name__)
CORS(attachment, resources=r"/api/attachment/*")

default_headers = {"Access-Control-Allow-Credentials": "true"}

cloudinary.config(
    cloud_name=CLOUD_NAME,
    api_key=CLOUDINARY_KEY,
    api_secret=CLOUDINARY_SECRET
)


@attachment.before_request
def before_request():
    pass


@attachment.route("/upload", methods=["POST"])
@jwt_required
def upload():
    """
    Uploads an attachment file to Cloudinary.
    """
    file = request.files["file"]

    if file and allowed_file_upload(file.filename):
        try:
            filename = secure_filename(file.filename)
            extension = get_file_extension(file.filename)

            new_filename = "".join(random.choices(string.ascii_letters + string.digits, k=12))
            new_filename_ext = ".".join((new_filename, extension))
            folder = "notes/attachments"
            root_dir = os.path.abspath(os.curdir)

            if FLASK_ENV == "development":
                saved_file = os.path.join(root_dir, "backend", "uploads", file.filename)
            else:
                saved_file = os.path.join(root_dir, "uploads", file.filename)

            file.save(saved_file)

            cloudinary.uploader.upload(saved_file,
                folder = folder,
                public_id = new_filename,
                overwrite = True)

            os.remove(saved_file)

            res = {"status": "OK", "filename": new_filename_ext }
            return Response(json.dumps(res)), 200, default_headers
        except:
            res = {"status": "Error with upload"}
            return Response(json.dumps(res)), 400, default_headers
    else:
        res = {"status": "Error with file"}
        return Response(json.dumps(res)), 400, default_headers


@attachment.route("/delete", methods=["POST"])
@jwt_required
def delete():
    """
    Deletes an attachment file on Cloudinary.
    """
    data = request.get_json(force=True, silent=False)

    try:
        filename = data["filename"]
        file_location = "/".join(("notes", "attachments", filename))

        cloudinary.uploader.destroy(file_location)

        res = {"status": "OK", "filename": filename }
        return Response(json.dumps(res)), 200, default_headers

    except:
        res = {"status": "Error with deletion"}
        return Response(json.dumps(res)), 400, default_headers
