import json

from flask import request, Blueprint, Response, jsonify
from flask_cors import CORS
from flask_bcrypt import Bcrypt

from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, \
    jwt_refresh_token_required, get_jwt_identity, set_access_cookies, set_refresh_cookies, \
    unset_jwt_cookies)

from src.models import User as UserModel

bcrypt = Bcrypt()

auth = Blueprint("auth", __name__)
CORS(auth, resources=r"/api/auth/*")

default_headers = {"Access-Control-Allow-Credentials": "true"}


@auth.before_request
def before_request():
    pass


@auth.route("/register", methods=["POST"])
def register():
    """
    Registers a new user.
    """
    data = request.get_json(force=True, silent=False)

    if all(k in data for k in ["username", "email", "password"]):
        username = data["username"].lower()
        email = data["email"].lower()
        password = data["password"]

        exist_username = UserModel.objects.filter(username=username).first()
        exist_email = UserModel.objects.filter(email=email).first()

        if exist_username:
            res = {"status": "Error", "message": "Username is already taken"}
            return Response(json.dumps(res)), 409, default_headers

        elif exist_email:
            res = {"status": "Error", "message": "Email address is already taken"}
            return Response(json.dumps(res)), 409, default_headers

        else:
            password_hash = bcrypt.generate_password_hash(password).decode("utf-8")
            new_user = UserModel(
                username=username,
                email=email,
                password=password_hash
            )
            new_user.save()

            res = {"status": "OK", "message": "User has successfully registered"}
            return Response(json.dumps(res)), 200, default_headers
    else:
        res = {"status": "Error", "message": "Please provide all required fields"}
        return Response(json.dumps(res)), 400, default_headers


@auth.route("/login", methods=["POST"])
def login():
    """
    Logs the user in.
    """
    data = request.get_json(force=True, silent=False)
    username = data["username"].lower()
    password = data["password"]
    current_user = UserModel.objects.filter(username=username).first()

    if current_user and bcrypt.check_password_hash(current_user.password, password):
        access_token = create_access_token(identity=username)
        refresh_token = create_refresh_token(identity=username)
        res = jsonify({"status": "OK", "message": "User logged in", "user": username})
        set_access_cookies(res, access_token)
        set_refresh_cookies(res, refresh_token)
        return res, 200, default_headers

    else:
        res = {"status": "Error", "message": "Wrong login credentials"}
        return Response(json.dumps(res)), 401, default_headers


@auth.route("/check", methods=["GET"])
@jwt_required
def check():
    """
    Checks if a user is still logged in.
    """
    try:
        user = get_jwt_identity()
        res = jsonify({"status": "OK", "message": "User is still logged in", "user": user})
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "No user is logged in"}
        return Response(json.dumps(res)), 401, default_headers


@auth.route("/refresh", methods=["POST"])
@jwt_refresh_token_required
def refresh():
    """
    Refreshes a valid JWT.
    """
    username = get_jwt_identity()
    access_token = create_access_token(identity=username)
    res = jsonify({"status": "OK", "message": "JWT refreshed"})
    set_access_cookies(res, access_token)
    return res, 200, default_headers


@auth.route("/logout", methods=["GET"])
def logout():
    """
    Logs the user out.
    """
    res = jsonify({"status": "OK", "message": "User logged out"})
    unset_jwt_cookies(res)
    return res, 200, default_headers
