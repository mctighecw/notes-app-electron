def get_file_extension(filename):
    """
    Gets the file extension.
    """
    return "." in filename and filename.rsplit(".", 1)[1].lower()


def allowed_file_upload(filename):
    """
    Allowed image types for picture upload.
    """
    ALLOWED_EXTENSIONS = set(["pdf", "png", "gif", "jpg", "jpeg"])
    extension = get_file_extension(filename)
    return extension in ALLOWED_EXTENSIONS
