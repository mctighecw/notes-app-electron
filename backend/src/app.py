import logging
import os

from flask import Flask
from flask_cors import CORS
from flask_mongoengine import MongoEngine
from flask_jwt_extended import JWTManager

from src.graphql.schema import schema
from src.graphql.graphql_view import graphql_view

from src import flask_config
from src.env_config import SECRET_KEY
from src.routes.auth import auth
from src.routes.attachment import attachment

def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)

    FLASK_ENV = os.getenv('FLASK_ENV')

    if FLASK_ENV == 'development':
        CORS(app, supports_credentials=True)

    logging.basicConfig(format='%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s',
                        level=app.config['LOG_LEVEL'])

    db = MongoEngine(app)

    if FLASK_ENV == 'development':
        secure_cookies = False
    else:
        secure_cookies = True

    app.config["JWT_TOKEN_LOCATION"] = ["cookies"]
    app.config["JWT_COOKIE_SECURE"] = secure_cookies
    app.config["JWT_ACCESS_COOKIE_PATH"] = "/api/"
    app.config["JWT_REFRESH_COOKIE_PATH"] = "/api/auth/refresh"
    app.config["JWT_COOKIE_CSRF_PROTECT"] = False
    app.config["JWT_SECRET_KEY"] = SECRET_KEY

    jwt = JWTManager(app)

    app.register_blueprint(auth, url_prefix="/api/auth")
    app.register_blueprint(attachment, url_prefix="/api/attachment")

    app.add_url_rule(
        '/api/graphql',
        view_func=graphql_view()
    )

    return app
