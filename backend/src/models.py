from datetime import datetime, timezone

from mongoengine import Document
from mongoengine.fields import StringField, ListField, DateTimeField


class User(Document):
    meta = { 'collection': 'users' }

    username = StringField()
    email = StringField()
    password = StringField()


class Note(Document):
    meta = { 'collection': 'data' }

    content = StringField()
    status = StringField(default='active')
    user = StringField()
    attachments = ListField(StringField())
    created_at = DateTimeField(default=datetime.now(timezone.utc).astimezone())
    deleted_at = DateTimeField()
