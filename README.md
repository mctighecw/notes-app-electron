# README

Users can sign up and log in, then take notes using rich text, special characters, and emojis. In addition, users can add attachments (images or PDFs). Notes can be sorted, archived, and deleted.

There are two versions: an online web app and an [Electron](https://electronjs.org) desktop version (for Mac, Linux, Windows).

## App Information

App Name: notes-app-electron

Created: September 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/notes-app-electron)

Production: [Link](https://notes-app.mctighecw.site)

## Tech Stack

Frontend:

- React (with React hooks)
- Sass (scss)
- CSS Modules
- Apollo GraphQL Client
- Prettier
- Electron

Backend:

- Flask
- JWT Authentication
- Graphene
- MongoDB
- Cloudinary (image management)

## To Run

### Locally

- Frontend

```
$ cd frontend
$ yarn install
$ yarn start
```

- Backend

```
$ cd backend
$ virtualenv venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
(venv) $ source run_dev.sh
```

- Database

  - Install and set up MongoDB with an admin user & password

  - Init db:

  ```
  $ source venv/bin/activate
  (venv) $ FLASK_ENV='development' python src/db/init_db.py
  ```

  - Drop db (if necessary):

  ```
  (venv) $ python src/db/drop_dev_db.py
  ```

  - Log into MongoDB locally:

  ```
  $ mongo -u admin --authenticationDatabase admin -p
  ```

### Docker

```
$ docker-compose up -d --build
$ docker exec -it notes-app-electron_mongodb_1 bash
  (set up admin user and password)
$ source init_docker_db.sh
```

Project running at `localhost:80`

### Electron

To start locally, install frontend packages and start backend locally (see above):

```
$ yarn start-electron
```

To build and make distribution version:

```
$ yarn build-electron
$ yarn dist-electron
```

## Credits

- Favicon and close icon with circle courtesy of Smashicons on [Flaticon](https://www.flaticon.com/authors/smashicons)
- Close icon courtesy of Hadrien on [Flaticon](https://www.flaticon.com/authors/hadrien)
- Search and info icons (magnifying glass) courtesy of freepik on [Flaticon](https://www.flaticon.com/authors/freepik)
- PDF icon courtesy of good-ware on [Flaticon](https://www.flaticon.com/authors/good-ware)
- Log out icon courtesy of dmitri13 on [Flaticon](https://www.flaticon.com/authors/dmitri13)
- Profile icon courtesy of Lucy G on [Flaticon](https://www.flaticon.com/authors/lucy-g)
- All other icons courtesy of Kiranshastry on [Flaticon](https://www.flaticon.com/authors/kiranshastry)

Last updated: 2024-12-19
